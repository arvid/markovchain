package org.centre4innovation.markov;

import java.util.*;

/**
 * A Markov chain is a stochastic model describing a sequence of possible events in which the probability of each event depends only on the state attained in the previous event.
 * This library allows one to train markov chains on generic sequences (for example Character, String, Integer,...). Use this trained model to:
 * <li> determine probabilities of unknown sequences.
 * <li> generate new random sequences.
 * @param <T> Sequence element type
 * @author Arvid Halma
 * @version 3-7-18
 */
@SuppressWarnings({"unused"})
public class MarkovChain<T> {

    private final MarkovTreeNode<T> root;
    private final MarkovTreeNode<T> starters;

    MarkovChain() {
        root = new MarkovTreeNode<>(null, null);
        starters = new MarkovTreeNode<>(null, null);
    }

    public void train(int depth, List<T> states) {
        final int stateCount = states.size();

        starters.addNext(states.get(0));

        for (int i = 0; i < stateCount; i++) {
            final int chainDepth = Math.min(depth, stateCount - i);

            MarkovTreeNode<T> chain = root;

            for (int k = 0; k < chainDepth; k++) {
                chain = chain.addNext(states.get(i + k));

                if (chainDepth != depth) {
                    chain.addEnd();
                }
            }
        }
    }

    public void train(int depth, T... states) {
        train(depth, Arrays.asList(states));
    }

    public double probability(int depth, T... states) {
        double logProbabilitySum = 0;
        int transitionCount = states.length - depth + 1;
        for (int i = 0; i < transitionCount; i++) {
            logProbabilitySum += Math.log(probabilityNgram(i, depth, states));
        }
        return Math.exp(logProbabilitySum / transitionCount);
    }


    private double probabilityNgram(int start, int n, T... states) {
        double p = 0;
        T state = states[start];
        MarkovTreeNode<T> chain = root.getNext(state);
        for (int i = start+1; i < start + n; i++) {
            if (chain == null) {
                return 0.0;
            }
            state = states[i];
            p += Math.log(chain.getProbability(state));

            chain = chain.getNext(state);
        }

        return Math.exp(p);
    }

    public double minProb(){
        return minProb(root, 1.0);
    }

    private double minProb(MarkovTreeNode<T> chain, double prior){
        if(chain == null || chain.next == null){
            return 1.0;
        }
        double pMin = prior;
        for (MarkovTreeNode<T> b : chain.next.values()) {
           double p = (double)b.occurrences / chain.nextTotalOccurrences;
           pMin = Math.min(p, prior * minProb(b, p));
        }
        return pMin;
    }

    public List<T> generate(Random random, T initialState, int min, int max) {
        int chainRoot = 0;
        int chainLength = 0;
        MarkovTreeNode<T> chain = root.getNext(initialState);

        List<T> out = new ArrayList<>(min);
        while (chainLength < max) {
            out.add(chainLength, chain.getState());

            while (chain == null || !chain.hasNext()) {
                T rootState;

                if (chainRoot == chainLength + 1) {
                    rootState = getRandom(random);
                } else {
                    rootState = out.get(++chainRoot);
                }

                chain = root.getNext(rootState);

                for (int k = chainRoot + 1; k <= chainLength && chain != null; k++) {
                    chain = chain.getNext(out.get(k));
                }
            }

            chainLength++;

            if (min > 0 && chainLength > min && chain.isEnd() && chain.isEnd(random)) {
                break;
            }

            chain = chain.getRandom(random);
        }

        return out;
    }

    public List<T> generate(Random random, int length) {
        return generate(random, getRandom(random), length, length);
    }

    public List<T> generate(Random random, T initialState, int length) {
        return generate(random, initialState, length, length);
    }

    public T getRandom(Random random) {
        return getRandom(root, random);
    }

    public T getRandom(Random random, T[] previousStates, int previousStateCount) {
        MarkovTreeNode<T> r = root;

        for (int i = 0; i < previousStateCount; i++) {
            MarkovTreeNode<T> n = r.getNext(previousStates[i]);

            if (n == null || !n.hasNext()) {
                break;
            }

            r = n;
        }

        return getRandom(r, random);
    }

    public T getRandom(MarkovTreeNode<T> chain, Random random) {
        MarkovTreeNode<T> r = chain.getRandom(random);

        return (r != null ? r.getState() : null);
    }

    public T getRandomStart(Random random) {
        return getRandom(starters, random);
    }

    @Override
    public String toString() {
        return root.toString();
    }

    /**
     * Mapping from a state to possible next states
     * @param <T> Element type
     * @author Arvid Halma
     * @version 3-7-18
     */
    private static class MarkovTreeNode<T> {

        final T state;
        final MarkovTreeNode<T> parent;
        int occurrences;
        Map<T, MarkovTreeNode<T>> next;
        int nextTotalOccurrences;
        int ends;

        MarkovTreeNode(T state, MarkovTreeNode<T> parent) {
            this.state = state;
            this.parent = parent;
        }

        T getState() {
            return state;
        }

        public int getOccurrences() {
            return occurrences;
        }

        void addOccurrence() {
            occurrences++;
        }

        MarkovTreeNode<T> addNext(T nextValue) {
            if (next == null) {
                next = new HashMap<>();
            }

            MarkovTreeNode<T> chain = next.get(nextValue);

            if (chain == null) {
                chain = new MarkovTreeNode<>(nextValue, this);
                next.put(nextValue, chain);
            }

            chain.addOccurrence();
            nextTotalOccurrences++;

            return chain;
        }

        MarkovTreeNode<T> getRandom(Random random) {
            if (next == null) {
                return null;
            }

            int i = random.nextInt(nextTotalOccurrences);

            for (MarkovTreeNode<T> n : next.values()) {
                i -= n.occurrences;

                if (i <= 0) {
                    return n;
                }
            }

            return null;
        }

        double getProbability(T value) {
            if(next == null)
                return 0.0;
            MarkovTreeNode<T> n = next.get(value);

            return (n == null ? 0.0 : (double) n.occurrences / (double) nextTotalOccurrences);
        }

        void addEnd() {
            ends++;
        }

        boolean isEnd(Random random) {
            return random.nextInt(ends + nextTotalOccurrences) < ends;
        }

        boolean isEnd() {
            return (ends > 0);
        }

        MarkovTreeNode<T> getNext(T value) {
            return next == null ? null : next.get(value);
        }

        boolean hasNext() {
            return (next != null && !next.isEmpty());
        }

        public boolean hasNext(T value) {
            return next.containsKey(value);
        }

        @Override
        public String toString() {
            return state + "("+occurrences+"): " + (next == null ? "" : next.values());

        }
    }
}
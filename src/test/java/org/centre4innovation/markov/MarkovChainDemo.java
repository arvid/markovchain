package org.centre4innovation.markov;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * @author Arvid Halma
 * @version 3-7-18
 */
public class MarkovChainDemo {
    final static String s = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
    /*final static String s = "Excited to see @Microsoft NL pick up the hololens app we've built together with @LUMC_Leiden & @we_are_inspark, into this snappy promotional film! We're proud of the impact this project has on students and educators.\n" +
            "Are you a teacher looking to spark new ideas for education? Join @LDECEL for their free workshop on September 25th.\n" +
            "Teachers with fresh ideas or perhaps a need for inspiration, join the free workshop 'Kick-start your course redesign' from the Leiden-Delft-Erasmus Centre for Education and Learning, 25 september 9:00-13:30 in Leiden. http://www.leiden-delft-erasmus.nl/nl/agenda/2018\n" +
            "The #mindoftheuniverse #MOOC explores the world of DNA, AI, and science's big questions. It's already inspired 1000s of learners and it begins again on Sep. 3. Enrol here\n" +
            "Looking forward to sharing the insights from this conference! #HumanityX\n" +
            "'Refugees have nuanced views on privacy and information sensitivity. Response organizations must protect the privacy rights of refugees and understand that different technologies receive different degrees of trust.'\n" +
            "'Students felt more confident going ‘into the field’ after going through a VR experience. They had a much clearer understanding of what was in store.'\n" +
            "Meer\n" +
            "\uD83D\uDCA1Interested in how innovation around #opendata and #bigdata can further #development? Join  #HumanityX and @TheSpindleNL @HumanityHub monthly Future Session on August 28th! \uD83D\uDDD3️ This month feat'ng @WUR @godanSec Sander Jansen and Rob Lokers.  Sign Up! \uD83D\uDC49\n" +
            "Explore DNA, AI, and science's \n" +
            " big questions \uD83E\uDD14in the #MindoftheUniverse #MOOC!  A great collaboration between our #DigitalLearning for @UniLeiden @erasmusuni @tudelft.  Enrol now on @coursera to begin on Sep. 3 \uD83D\uDC49\n" +
            "\uD83D\uDCA1Interested in how innovation around #opendata and #bigdata can further #development? Join  #HumanityX and @TheSpindleNL @HumanityHub monthly Future Session\n" +
            "on August 28th! \uD83D\uDDD3️ This month feat'ng @WUR @godanSec Sander Jansen and Rob Lokers.  Sign Up! \uD83D\uDC49\n" +
            "August 19 is the @UN World Humanitarian Day, to draw attention to the millions of aid workers and civilians affected by conflict. \uD83D\uDDFA️\uD83D\uDD4A️See #NotATarget for more, or visit  \n" +
            "https://www.worldhumanitarianday.org/en\n" +
            "Exploring #blockchain for your #humanitarian organization? Looking for a guide through the uses of the technology? We've co-developed a useful decision tree which goes through your use case step-by-step. Click through here \uD83D\uDC49 https://goo.gl/Ng57Yk   #HumanityX @HumTechLab\n" +
            "Weekend listening! @BarrThomas in discussion with Chris Hoffman on #humanitarianinnovation! \uD83D\uDCA1 #HumanityX #peoplefirst\n" +
            "Active classrooms, by teachers! Putting #peoplefirst in design, (re)making space for flexible, adaptive learning\n" +
            "Our next #360VR app development is underway with @LUMC_Leiden, to prepare medical interns to navigate through acute situations \uD83E\uDD12\uD83D\uDC69\u200D⚕️\uD83D\uDC68\u200D⚕️\n" +
            "#DigitalLearning #VRforEducation\n" +
            "Innovation has to happen in collaboration with users' experience. This is a nice example of how to do that!  \n" +
            "Humanitarian innovators! Only 7 days left to submit your innovative technological solutions to one of the three challenges in the Humanitarian Action Challenge @HagueInnovators @GemeenteDenHaag. Win funding plus support to pilot! Read more and apply \uD83D\uDC49\n" +
            "Got it in 1 take! \uD83C\uDF9E️Our latest #MOOC about the theory and practice of #IHL is really coming together \uD83D\uDC69\u200D\uD83C\uDF93@KGFLeiden\n" +
            "Really excited to present the interactive Blockchain Decision Tree by https://www.linkedin.com/in/kate-dodgson-400bb366/ … for our #HumanityX lab and @HumTechLab. ⛓️\uD83C\uDF33Resolve #humanitarian organisation \"should we blockchain?!\" questions, accounting for humanitarian & dev. contexts \uD83D\uDC49";
*/
    public static void main(String[] args) {
        demo(2);
        demo(4);
//        demo(5);
//        demo(6);
    }

    public static void demo(int n) {
        MarkovChain<Character> markovChain = new MarkovChain<>();
        markovChain.train(n, characters(s));
        System.out.println("tree = " + markovChain);
        System.out.println("markov.minProb() = " + markovChain.minProb());

        Arrays.stream(new String[]{"Lorem", "ipsum", "lolpreat", "arvid", "donini", "123"}).forEach(word -> {
            for (int i = n; i >= 1; i--) {
                System.out.printf("prob_k=%d( %s ) = %.4f\n", i, word, markovChain.probability(i, characters(word)));
            }
            System.out.println();
        });

        System.out.println();
        for (int i = 0; i < 100; i++) {
            System.out.println("\n" + string(markovChain.generate(new Random(i), 160)));
        }
    }


    public static Character[] characters(String s) {
        Character[] result = new Character[s.length()];
        for (int i = 0; i < result.length; i++) {
             result[i] = s.charAt(i);
        }
        return result;
    }

    public static String string(Character[] cs) {
        char[] result = new char[cs.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = cs[i];
        }
        return new String(result);
    }

    public static String string(List<Character> cs) {
        char[] result = new char[cs.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = cs.get(i);
        }
        return new String(result);
    }
}
